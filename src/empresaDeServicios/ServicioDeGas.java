package empresaDeServicios;

public abstract class ServicioDeGas extends Servicio {
	/* esta variable solo debe estar en el padre */
	private int cantidadDeArtefactos;

	public ServicioDeGas(String tipoDeServicio, String domicilioDestino, Cliente cliente, Especialista especialista,
			int cantidadDeArtefactos) {
		super(tipoDeServicio, domicilioDestino, cliente, especialista);
		if (cantidadDeArtefactos <= 0) {
			throw new RuntimeException("No existen artefactos");
		}
			this.cantidadDeArtefactos = cantidadDeArtefactos;
	}

	public int cantidadDeArtefactos() {
		return cantidadDeArtefactos;
	}

}
