package empresaDeServicios;

public class ServicioRevisioDeGas extends ServicioDeGas {
	/** revision 10 pociente >5 revision 15 pociente >10 */
	private static double descuentoPorMayorACincoArtefactos = 0.1;
	private static double descuentoPorMayorADiezArtefactos = 0.15;
	private double costoPorRevision;

	public ServicioRevisioDeGas(String tipoDeServicio, String domicilioDestino, Cliente cliente,
			Especialista especialista, int cantidadDeArtefactos, double costoPorRevision) {
		super(tipoDeServicio, domicilioDestino, cliente, especialista, cantidadDeArtefactos);
		validar(costoPorRevision);
		this.costoPorRevision = costoPorRevision;
	}

	private void validar(double costoPorRevision2) {
		if (costoPorRevision2 <= 0) {
			throw new RuntimeException("valores incorrectos, deben ser >0");
		}
	}

	@Override
	public double finalizarServicio(double costoMateriales) {
		if (!super.servicioActivo()) {
			throw new RuntimeException("El servicio ya fue finalizado");
		} else {
			super.cambiaAEstadoFinalizado();
			return calcularCosto()+costoMateriales;
		}
	}

	private double calcularConDescuentoDelQuincePociento() {
		return costoPorRevision * super.cantidadDeArtefactos()
				- ((super.cantidadDeArtefactos() * costoPorRevision) * descuentoPorMayorADiezArtefactos) ;
	}

	private double calcularConDescuentoDelDiezPorciento() {
		return costoPorRevision * super.cantidadDeArtefactos() 
				- ((super.cantidadDeArtefactos() * costoPorRevision) * descuentoPorMayorACincoArtefactos) ;
	}
	/* mejorar el finalizar servicio con otro metodo que sume los costo de servicio y relice su validaciones antes*/

	@Override
	public String tipoDeServicio() {
		return super.especialistaAsignado().tipoDeEspecialidad();
	}
	@Override
	public String toString() {
		
		return " + [ "+super.numeroDeServicio()+ " - "+ tipoDeServicio() +" ] "+ super.domicillio() ; 
	}

	@Override
	public double calcularCosto() {
		if (super.cantidadDeArtefactos() > 5 && super.cantidadDeArtefactos() <= 10) {
			return calcularConDescuentoDelDiezPorciento();
		} else if (super.cantidadDeArtefactos() > 10) {
			return calcularConDescuentoDelQuincePociento();
		}
		return super.cantidadDeArtefactos() * costoPorRevision;
	
	}

}
