package empresaDeServicios;

public abstract class Servicio {
	private static int contador = 0;
	private int idServicio;
	private String tipoDeServicio;
	private Cliente cliente;
	private Especialista especialista;
	private String domicilioDestino;
	private boolean servicioActivo;

	public Servicio(String tipoDeServicio, String domicilioDestino, Cliente cliente, Especialista especialista) {

		this.tipoDeServicio = tipoDeServicio;
		this.domicilioDestino = domicilioDestino;
		this.cliente = cliente;
		this.servicioActivo = true;
		this.especialista = especialista;
		this.idServicio = generarCodigoUnico();
	}

	private static synchronized int generarCodigoUnico() {
		return contador++;
	}
	public abstract double calcularCosto();
	/* metodos */
	public abstract double finalizarServicio(double costoMateriales);

	public void cambiaAEstadoFinalizado() {
		servicioActivo = false;
	}

	public boolean servicioActivo() {
		return this.servicioActivo;
	}

	public int numeroDeServicio() {
		return this.idServicio;
	}

	public Especialista especialistaAsignado() {
		return this.especialista;
	}

	public void cambiarEspecialista(Especialista nuevoEspecialista) {
		if (!this.especialista.equals(nuevoEspecialista)) {
			this.especialista = null;
			this.especialista = nuevoEspecialista;
		} else {
			throw new RuntimeException("No es posible cambiar por a si mismo");
		}
	}

	public String domicillio() {
		return domicilioDestino;
	}

	public abstract String tipoDeServicio();

	public boolean esEspecialistaDeServicio(Especialista especialista2) {
		return this.especialista.equals(especialista2);
	}

}