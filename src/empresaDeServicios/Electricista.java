package empresaDeServicios;

import javax.management.RuntimeErrorException;

public class Electricista extends Servicio {
	private int horaDeTrabajo;
	private double valorPorHora;

	public Electricista(String tipoServicio, String domicioDestino, Cliente cliente, Especialista especialista,
			double precioPorHora, int horasDeTrabajo) {
		super(tipoServicio, domicioDestino, cliente, especialista);
		validarRemuneraciones(precioPorHora, horasDeTrabajo);
		this.valorPorHora = precioPorHora;
		this.horaDeTrabajo = horasDeTrabajo;
	}

	private void validarRemuneraciones(double precioPorHora, int horasDeTrabajo) {
		if (precioPorHora <= 0 || horasDeTrabajo <= 0) {
			throw new RuntimeException("Los valores son negativos");
		}
	}

	@Override
	public double finalizarServicio(double costoMateriales) {
		if (!super.servicioActivo()) {
			throw new RuntimeException("El servicio ya fue finalizado");
		} else {
			super.cambiaAEstadoFinalizado();
			return costoMateriales + calcularCosto();
		}
	}

	@Override
	public String tipoDeServicio() {
		return super.especialistaAsignado().tipoDeEspecialidad();
	}
	@Override
	public String toString() {
		return "  + [ "+super.numeroDeServicio()+ " - "+ tipoDeServicio() +" ] "+ super.domicillio() ; 
	}

	@Override
	public double calcularCosto() {
		return horaDeTrabajo * valorPorHora;
	}
}
