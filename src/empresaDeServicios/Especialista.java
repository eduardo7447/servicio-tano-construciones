package empresaDeServicios;

public class Especialista {
	private int numId;
	private String nombre ;
	private String numeroTelefono;
	private String especialidadEnServicio;
	


	public Especialista(int numId, String nombre, String numeroTelefono, String especialidadEnServicio) {
		this.numId = numId;
		this.nombre = nombre;
		this.numeroTelefono = numeroTelefono;
		this.especialidadEnServicio = especialidadEnServicio;
	}
	/* este metodo se realiza en la empresa preguntar si corresponde cambiar el especialista
	 */
//	public void cambiarEspecialista(Especialista e) {
//				if(!this.equals(e)) {
//					
//				}
//	}
	public int numeroId() {
		return numId;
	}
	public boolean esDeTipo(String especialidadDeServicio) {		
		return especialidadEnServicio.equals(especialidadDeServicio);
	}
	@Override
	public boolean equals(Object objeto) {
		if(objeto == null) {
			return false;
		}
		if(objeto.getClass().equals(this.getClass())) {
			Especialista especialista = (Especialista)objeto;
			return (this.numId== especialista.numId);
		}
		return false;
	}
	@Override
	public int hashCode() {
		return (11*(11+numId));
	}
	public String tipoDeEspecialidad() {
		return this.especialidadEnServicio;
	}

}
