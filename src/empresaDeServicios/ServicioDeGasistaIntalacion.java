package empresaDeServicios;

public class ServicioDeGasistaIntalacion extends ServicioDeGas {
	/* el precio por artefacto debe ser fijo */
	private double precioPorArtefactoInstalado;

	public ServicioDeGasistaIntalacion(String tipoDeServicio, String domicilioDestino, Cliente cliente,
			Especialista especialista, int cantidadDeArtefactos, double precioPorArtefacto) {
		super(tipoDeServicio, domicilioDestino, cliente, especialista, cantidadDeArtefactos);
		validar(precioPorArtefacto);
		this.precioPorArtefactoInstalado = precioPorArtefacto;
	}

	private void validar(double precioPorArtefacto) {
		if (precioPorArtefacto <= 0) {
			throw new RuntimeException("El precio debe ser positivo");
		}
	}

	@Override
	public double finalizarServicio(double costoMateriales) {
		if (!super.servicioActivo()) {
			throw new RuntimeException("El servicio ya fue finalizado");
		} else {
			super.cambiaAEstadoFinalizado();
			return costoMateriales + calcularCosto();
		}
	}

	@Override
	public String tipoDeServicio() {
		return super.especialistaAsignado().tipoDeEspecialidad();
	}
	@Override
	public String toString() {
		return " + [ "+super.numeroDeServicio()+ " - "+ tipoDeServicio() +" ] "+ super.domicillio() ; 
	}

	@Override
	public double calcularCosto() {
		return super.cantidadDeArtefactos() * precioPorArtefactoInstalado;
	}

}
