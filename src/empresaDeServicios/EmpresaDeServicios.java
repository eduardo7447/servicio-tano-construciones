package empresaDeServicios;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class EmpresaDeServicios implements Interfaz {

	HashMap<Integer, Servicio> servicioPorRealizar;
	HashMap<String, Integer> cantidadServiciosFinalizadosPorTipo;
	HashMap<Integer, Especialista> especialistasDisponibles;
	HashMap<Integer, Cliente> clientes;
	Set<String> tiposDeServicios;
	private HashMap<String, Integer> facturacionTotalPorTipo;
	private double facturacionTotal;

	public EmpresaDeServicios() {
		this.servicioPorRealizar = new HashMap<Integer, Servicio>();
		this.especialistasDisponibles = new HashMap<Integer, Especialista>();
		this.clientes = new HashMap<Integer, Cliente>();
		this.facturacionTotalPorTipo = new HashMap<String, Integer>();
		this.cantidadServiciosFinalizadosPorTipo = new HashMap<String, Integer>();
		this.facturacionTotal = 0;
		this.tiposDeServicios = new HashSet<>();
		this.tiposDeServicios.add("Electricidad");
		this.tiposDeServicios.add("Pintura");
		this.tiposDeServicios.add("GasistaInstalacion");
		this.tiposDeServicios.add("GasistaRevision");
		this.tiposDeServicios.add("PinturaEnAltura");
		/* para cada nombre de servicio inicializo su cant servicio realizadado en 0 */
		for (String ser : tiposDeServicios) {
			cantidadServiciosFinalizadosPorTipo.put(ser, 0);
		}
		/* para cada nombre de servicio inicializo su facturacion de servicio en 0 */
		for (String serParaFacturar : tiposDeServicios) {
			this.facturacionTotalPorTipo.put(serParaFacturar, 0);
		}
	}

	@Override
	public void registrarCliente(int dni, String nombre, String telefono) {

		/*
		 * para que funcionen los metodos de la colleccion de map es necesario
		 * implementar el hashcod con el equals ambos que comparen los mismos valores
		 */

		if (existeCliente(dni)) {
			throw new RuntimeException("El cliente ya esta registrado");
		} else {
			Cliente c = new Cliente(dni, nombre, telefono);
			clientes.put(dni, c);
		}
	}

	private boolean existeCliente(int dni) {
		return clientes.containsKey(dni);
	}

	@Override
	public void registrarEspecialista(int nroEspecialista, String nombre, String telefono, String especialidad) {
		validarDatosDeEspecialista(nroEspecialista, especialidad);
		Especialista e = new Especialista(nroEspecialista, nombre, telefono, especialidad);
		especialistasDisponibles.put(nroEspecialista, e);
	}

	private void validarDatosDeEspecialista(int nroEspecialista, String especialidad) {
		if (existeEspecialista(nroEspecialista) || tipoEspecialidadDesconocida(especialidad)) {
			throw new RuntimeException("El especialista no puede ser registrado");
		}
	}

	private boolean tipoEspecialidadDesconocida(String especialidad) {
		boolean ret = false;
		for (String tipoServicio : tiposDeServicios) {
			ret = ret || tipoServicio.equals(especialidad);
		}
		return !tiposDeServicios.contains(especialidad);
	}

	private boolean existeEspecialista(int nroEspecialista) {
		return especialistasDisponibles.containsKey(nroEspecialista);
	}

	@Override
	public int solicitarServicioElectricidad(int dni, int nroEspecialista, String direccion, double precioPorhora,
			int horasTrabajadas) {

		Especialista e = validarEspecialista(nroEspecialista);
		Cliente c = validarCliente(dni);
		validarEspecialidadDeServicio(e, "Electricidad");
		Electricista electricista = new Electricista(e.tipoDeEspecialidad(), direccion, c, e, precioPorhora,
				horasTrabajadas);
		servicioPorRealizar.put(electricista.numeroDeServicio(), electricista);
		return electricista.numeroDeServicio();
	}

	private Cliente validarCliente(int dni) {
		if (!existeCliente(dni)) {
			throw new RuntimeException("El cliente no existe!");
		}
		return clientes.get(dni);
	}

	private Especialista validarEspecialista(int nroEspecialista) {
		if (!existeEspecialista(nroEspecialista)) {
			throw new RuntimeException("El especialista no existe!");
		}
		return this.especialistasDisponibles.get(nroEspecialista);
	}

	@Override
	public int solicitarServicioPintura(int dni, int nroEspecialista, String direccion, int metrosCuadrados,
			double precioPorMetroCuadrado) {

		Especialista e = validarEspecialista(nroEspecialista);
		Cliente c = validarCliente(dni);
		validarEspecialidadDeServicio(e, "Pintura");
		ServicioPintura servicioPintura = new ServicioPintura(e.tipoDeEspecialidad(), direccion, c, e, metrosCuadrados,
				precioPorMetroCuadrado);
		servicioPorRealizar.put(servicioPintura.numeroDeServicio(), servicioPintura);
		return servicioPintura.numeroDeServicio();
	}

	@Override
	public int solicitarServicioPintura(int dni, int nroEspecialista, String direccion, int metrosCuadrados,
			double precioPorMetroCuadrado, int piso, double seguro, double alqAndamios) {

		Especialista e = validarEspecialista(nroEspecialista);
		Cliente c = validarCliente(dni);
		validarEspecialidadDeServicio(e, "PinturaEnAltura");
		ServicioPinturaEnAltura servicioPinturaEnAltura = new ServicioPinturaEnAltura(e.tipoDeEspecialidad(), direccion,
				c, e, metrosCuadrados, precioPorMetroCuadrado, seguro, alqAndamios, piso);
		servicioPorRealizar.put(servicioPinturaEnAltura.numeroDeServicio(), servicioPinturaEnAltura);
		return servicioPinturaEnAltura.numeroDeServicio();
	}

	@Override
	public int solicitarServicioGasistaInstalacion(int dni, int nroEspecialista, String direccion, int cantArtefactos,
			double precioPorArtefacto) {

		Especialista e = validarEspecialista(nroEspecialista);
		Cliente c = validarCliente(dni);
		validarEspecialidadDeServicio(e, "GasistaInstalacion");//
		ServicioDeGasistaIntalacion servicioGasistaInstalacion = new ServicioDeGasistaIntalacion(e.tipoDeEspecialidad(),
				direccion, c, e, cantArtefactos, precioPorArtefacto);
		servicioPorRealizar.put(servicioGasistaInstalacion.numeroDeServicio(), servicioGasistaInstalacion);
		return servicioGasistaInstalacion.numeroDeServicio();
	}

	@Override
	public int solicitarServicioGasistaRevision(int dni, int nroEspecialista, String direccion, int cantArtefactos,
			double precioPorArtefacto) {
		Especialista e = validarEspecialista(nroEspecialista);
		Cliente c = validarCliente(dni);
		validarEspecialidadDeServicio(e, "GasistaRevision");
		ServicioRevisioDeGas servicioRevisionDeGas = new ServicioRevisioDeGas(e.tipoDeEspecialidad(), direccion, c, e,
				cantArtefactos, precioPorArtefacto);
		this.servicioPorRealizar.put(servicioRevisionDeGas.numeroDeServicio(), servicioRevisionDeGas);
		return servicioRevisionDeGas.numeroDeServicio();
	}

	@Override
	public double finalizarServicio(int codServicio, double costoMateriales) {
		Servicio s = validarServicio(codServicio);//										O(1)
		int precioDeFacturacionAlClliente = (int) s.finalizarServicio(costoMateriales);//	O(1)
		facturacionTotal += precioDeFacturacionAlClliente;//								O(1)
		cantidadServiciosFinalizadosPorTipo.put(s.tipoDeServicio(),
				cantidadServiciosFinalizadosPorTipo.get(s.tipoDeServicio()) + 1);//			O(1)
		this.facturacionTotalPorTipo.put(s.tipoDeServicio(),
				facturacionTotalPorTipo.get(s.tipoDeServicio()) + precioDeFacturacionAlClliente);//	O(1)
		return precioDeFacturacionAlClliente;//														O(1)
		
	}

	private Servicio validarServicio(int codServicio) {
		if (!servicioValido(codServicio)) {
			throw new RuntimeException("Servicio invalido");
		}
		return servicioPorRealizar.get(codServicio);
	}

	@Override
	public Map<String, Integer> cantidadDeServiciosRealizadosPorTipo() {
		Map<String, Integer> cantidadServiciosPorCadaTipo = (Map<String, Integer>) this.cantidadServiciosFinalizadosPorTipo
				.clone();
		return cantidadServiciosPorCadaTipo;
	}

	@Override
	public double facturacionTotalPorTipo(String tipoServicio) {
		return this.facturacionTotalPorTipo.get(tipoServicio);
	}

	@Override
	public double facturacionTotal() {
		return facturacionTotal;
	}

	@Override
	public void cambiarResponsable(int codServicio, int nroEspecialista) {
		Servicio s = validarServicio(codServicio);
		Especialista especialista = validarEspecialista(nroEspecialista);
		validarEspecialidadDeServicio(s.especialistaAsignado(), especialista.tipoDeEspecialidad());
		s.cambiarEspecialista(especialista);
	}

	private boolean servicioValido(int codServicio) {
		return servicioPorRealizar.containsKey(codServicio);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Servicio s : servicioPorRealizar.values()) {
			sb.append(s);
		}
		return sb.toString();
	}

	@Override
	public String listadoServiciosAtendidosPorEspecialista(int nroEspecialita) {
		Especialista especialista = validarEspecialista(nroEspecialita);/*		O(1)*/
		StringBuilder sb = new StringBuilder();/*								O(1)*/
		if (especialistaRealizoCeroServicios(especialista)) {/*                 O(n)*/
			return sb.toString();/*												O(1)*/
		} else {
			/*para el peor caso que no exista servicios realiuzados por el especialista:
			 * entrara al primer condicional, de caso contrario ingresara al for. En el su peor caso es:
			 * que el especialista fue el que realizo todos los servicios disponibles.
			 * 	CUALQUIERA SEA EL PEOR DE LOS CASOS O(n)*/
			for (Servicio s : servicioPorRealizar.values()) {/*                 O(n)*/
				if (s.esEspecialistaDeServicio(especialista)){/*  O(1)*O(n)*/
					sb.append(s).append("\n");/*                 O(1)*O(n)*/
			}
		}
		return sb.toString();/*                 								O(1)*/
		/*ANALISIS DEL CASO DEL FOR ENTRA EN TODOS LOS SERVICIOS REALIZADOS
		 * O(o(n)+O(1)*O(n)+O(1)*O(n)+O(1))
		 * */
		}
	}

	private boolean especialistaRealizoCeroServicios(Especialista especialista) {
		boolean ret = false;
		for (Servicio s : servicioPorRealizar.values()) {
			ret |= s.esEspecialistaDeServicio(especialista);
		}
		return !ret;
	}

	/* Metodos generales */
	private void validarEspecialidadDeServicio(Especialista e, String tipoEspecialidad) {
		if (!e.esDeTipo(tipoEspecialidad))
			throw new RuntimeException("Especialista de tipo incorrecto");
	}

}
