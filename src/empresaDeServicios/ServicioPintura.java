package empresaDeServicios;

public class ServicioPintura extends Servicio {
	private double superficie;
	private double costoMetroCuadrado;

	public ServicioPintura(String tipoDeServicio, String domicioDestino, Cliente cliente, Especialista especialista,
			double superficie, double costoMetroCuadrado) {

		super(tipoDeServicio, domicioDestino, cliente, especialista);
		validarSuperficieYCostoMetroCuadrado(superficie, costoMetroCuadrado);
		this.superficie = superficie;
		this.costoMetroCuadrado = costoMetroCuadrado;
	}

	private void validarSuperficieYCostoMetroCuadrado(double superficie, double costoMetroCuadrado) {
		if (superficie <= 0 || costoMetroCuadrado <= 0) {
			throw new RuntimeException("valores incorrectos, deben ser >0!");
		}
	}

	@Override
	public double finalizarServicio(double costoMateriales) {
		if (!super.servicioActivo()) {
			throw new RuntimeException("El servicio ya fue finalizado");
		} else {
			super.cambiaAEstadoFinalizado();
			return calcularCosto() + costoMateriales;
		}
	}

	/* todos las clases hijas saben responder el mismo metodo de la clase padre,
	 * pero en particular cada una lo hace de un modo distinto
	 */
	@Override
	public String tipoDeServicio() {
		return super.especialistaAsignado().tipoDeEspecialidad();
	}
	@Override
	public String toString() {
		return " + [ "+super.numeroDeServicio()+ " - "+ tipoDeServicio() +" ] "+ super.domicillio() ; 
	}

	@Override
	public double calcularCosto() {
		return superficie * costoMetroCuadrado;
	}

}
