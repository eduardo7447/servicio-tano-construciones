package empresaDeServicios;

public class ServicioPinturaEnAltura extends ServicioPintura {

	private double costoSeguro;
	private double costoDeAlquillerDeAndamio;
	private int numeroDePiso;
	private static double porcentajeDeAumentoPorAndamio = 0.2;

	public ServicioPinturaEnAltura(String tipoDeServicio, String domicioDestino, Cliente cliente,
			Especialista especialista, double superficie, double costoMetroCuadrado, double costoSeguro,
			double costoDeAlquilerDeAndamio, int numeroDePiso) {

		super(tipoDeServicio, domicioDestino, cliente, especialista, superficie, costoMetroCuadrado);
		validarRemuneraciones(costoSeguro, costoDeAlquilerDeAndamio, numeroDePiso);
		this.costoSeguro = costoSeguro;
		this.costoDeAlquillerDeAndamio = costoDeAlquilerDeAndamio;
		this.numeroDePiso = numeroDePiso;
	}

	private void validarRemuneraciones(double costoSeguro, double costoDeAlquillerDeAndamio, int numeroDePiso) {
		if (costoSeguro < 1 || costoDeAlquillerDeAndamio < 1 || numeroDePiso < 1) {
			throw new RuntimeException("ingreso negativo invalido");
		}
	}
	// si el piso es mayor o igual a 1 se agregan el valor del andamio + costo de seguro
	 
	private boolean esServicioEnAltura() {
		return numeroDePiso >= 1;
	}

	/* ademas de agregar el valor del andamio+seguro, el andamio aumenta un 20% */
	private boolean esServicioEnAlturaMayorA5() {
		return this.numeroDePiso > 5;
	}

	private double incrementarPorcentajeAlValorAndamio() {
		return costoDeAlquillerDeAndamio +=(costoDeAlquillerDeAndamio * porcentajeDeAumentoPorAndamio);
	}

	@Override
	public double finalizarServicio(double costoMateriales) {
		if (!super.servicioActivo()) {
			throw new RuntimeException("El servicio ya fue finalizado");
		} else {
				super.cambiaAEstadoFinalizado();
				return costoMateriales+calcularCosto();
		}		
	}
	@Override
	public double calcularCosto() {
		verificarAlturaDePiso();
		return super.calcularCosto()+costoDeAlquillerDeAndamio + costoSeguro;
	}

	private void verificarAlturaDePiso() {
		if (esServicioEnAlturaMayorA5()) {
			incrementarPorcentajeAlValorAndamio();
		}
	}

	@Override
	public String tipoDeServicio() {
		return super.especialistaAsignado().tipoDeEspecialidad();
	}
	@Override
	public String toString() {
		return " + [ "+super.numeroDeServicio()+ " - "+ tipoDeServicio() +" ] "+ super.domicillio() ; 
	}
}